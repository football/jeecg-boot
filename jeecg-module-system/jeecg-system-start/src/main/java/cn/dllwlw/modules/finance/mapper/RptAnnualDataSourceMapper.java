package cn.dllwlw.modules.finance.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.dllwlw.modules.finance.entity.RptAnnualDataSource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 财务报表数据来源
 * @Author: jeecg-boot
 * @Date:   2022-11-08
 * @Version: V1.0
 */
public interface RptAnnualDataSourceMapper extends BaseMapper<RptAnnualDataSource> {

}
