package cn.dllwlw.modules.finance.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import cn.dllwlw.modules.finance.entity.RptAnnualDataSource;
import cn.dllwlw.modules.finance.service.IRptAnnualDataSourceService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 财务报表数据来源
 * @Author: jeecg-boot
 * @Date:   2022-11-08
 * @Version: V1.0
 */
@Api(tags="财务报表数据来源")
@RestController
@RequestMapping("/finance/rptAnnualDataSource")
@Slf4j
public class RptAnnualDataSourceController extends JeecgController<RptAnnualDataSource, IRptAnnualDataSourceService> {
	@Autowired
	private IRptAnnualDataSourceService rptAnnualDataSourceService;
	
	/**
	 * 分页列表查询
	 *
	 * @param rptAnnualDataSource
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "财务报表数据来源-分页列表查询")
	@ApiOperation(value="财务报表数据来源-分页列表查询", notes="财务报表数据来源-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<RptAnnualDataSource>> queryPageList(RptAnnualDataSource rptAnnualDataSource,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<RptAnnualDataSource> queryWrapper = QueryGenerator.initQueryWrapper(rptAnnualDataSource, req.getParameterMap());
		Page<RptAnnualDataSource> page = new Page<RptAnnualDataSource>(pageNo, pageSize);
		IPage<RptAnnualDataSource> pageList = rptAnnualDataSourceService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param rptAnnualDataSource
	 * @return
	 */
	@AutoLog(value = "财务报表数据来源-添加")
	@ApiOperation(value="财务报表数据来源-添加", notes="财务报表数据来源-添加")
	//@RequiresPermissions("cn.dllwlw.modules:rpt_annual_data_source:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody RptAnnualDataSource rptAnnualDataSource) {
		rptAnnualDataSourceService.save(rptAnnualDataSource);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param rptAnnualDataSource
	 * @return
	 */
	@AutoLog(value = "财务报表数据来源-编辑")
	@ApiOperation(value="财务报表数据来源-编辑", notes="财务报表数据来源-编辑")
	//@RequiresPermissions("cn.dllwlw.modules:rpt_annual_data_source:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody RptAnnualDataSource rptAnnualDataSource) {
		rptAnnualDataSourceService.updateById(rptAnnualDataSource);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "财务报表数据来源-通过id删除")
	@ApiOperation(value="财务报表数据来源-通过id删除", notes="财务报表数据来源-通过id删除")
	//@RequiresPermissions("cn.dllwlw.modules:rpt_annual_data_source:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		rptAnnualDataSourceService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "财务报表数据来源-批量删除")
	@ApiOperation(value="财务报表数据来源-批量删除", notes="财务报表数据来源-批量删除")
	//@RequiresPermissions("cn.dllwlw.modules:rpt_annual_data_source:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.rptAnnualDataSourceService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "财务报表数据来源-通过id查询")
	@ApiOperation(value="财务报表数据来源-通过id查询", notes="财务报表数据来源-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<RptAnnualDataSource> queryById(@RequestParam(name="id",required=true) String id) {
		RptAnnualDataSource rptAnnualDataSource = rptAnnualDataSourceService.getById(id);
		if(rptAnnualDataSource==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(rptAnnualDataSource);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param rptAnnualDataSource
    */
    //@RequiresPermissions("cn.dllwlw.modules:rpt_annual_data_source:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, RptAnnualDataSource rptAnnualDataSource) {
        return super.exportXls(request, rptAnnualDataSource, RptAnnualDataSource.class, "财务报表数据来源");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    //@RequiresPermissions("rpt_annual_data_source:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, RptAnnualDataSource.class);
    }

}
