package cn.dllwlw.modules.finance.service;

import cn.dllwlw.modules.finance.entity.RptAnnualDataSource;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 财务报表数据来源
 * @Author: jeecg-boot
 * @Date:   2022-11-08
 * @Version: V1.0
 */
public interface IRptAnnualDataSourceService extends IService<RptAnnualDataSource> {

}
