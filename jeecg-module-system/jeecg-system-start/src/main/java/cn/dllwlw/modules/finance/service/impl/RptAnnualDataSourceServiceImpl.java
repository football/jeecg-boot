package cn.dllwlw.modules.finance.service.impl;

import cn.dllwlw.modules.finance.entity.RptAnnualDataSource;
import cn.dllwlw.modules.finance.mapper.RptAnnualDataSourceMapper;
import cn.dllwlw.modules.finance.service.IRptAnnualDataSourceService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 财务报表数据来源
 * @Author: jeecg-boot
 * @Date:   2022-11-08
 * @Version: V1.0
 */
@Service
public class RptAnnualDataSourceServiceImpl extends ServiceImpl<RptAnnualDataSourceMapper, RptAnnualDataSource> implements IRptAnnualDataSourceService {

}
